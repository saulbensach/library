defmodule LibraryWeb.Router do
  use LibraryWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug Library.MethodPlug
  end

  pipeline :auth do
    plug Library.AuthPlug
  end

  scope "/api", LibraryWeb do
    pipe_through :api

    #BOOKS no auth
    get "/books",     BookController, :index
    get "/books/:id", BookController, :show
    get "/books/all/:id", BookController, :show_all
    
    #Users no auth
    get "/users",     UserController, :index
    get "/users/:id", UserController, :show

    #authors no auth
    get "/authors",     AuthorController, :index
    get "/authors/:id", AuthorController, :show
    get "/authors/all/:id", AuthorController, :show_all

    #Methods no auth
    get "/methods",     MethodController, :index
    get "/methods/:id", MethodController, :show

    pipe_through :auth
    #BOOKS!
    post    "/books",     BookController, :create
    patch   "/books/:id", BookController, :update
    put     "/books/:id", BookController, :update
    delete  "/books/:id", BookController, :delete

    #USERS!
    post    "/users",     UserController, :create
    #patch   "/users/:id", UserController, :update
    #put     "/users/:id", UserController, :update
    #delete  "/users/:id", UserController, :delete

    #Authors!
    post    "/authors",     AuthorController, :create
    patch   "/authors/:id", AuthorController, :update
    put     "/authors/:id", AuthorController, :update
    delete  "/authors/:id", AuthorController, :delete

    #Methods!
    #post    "/methods",     MethodController, :create
    #patch   "/methods/:id", MethodController, :update
    #put     "/methods/:id", MethodController, :update
    #delete  "/methods/:id", MethodController, :delete
  end
end
