defmodule Library.MethodPlug do
    import Plug.Conn

    alias Library.MethodPlug
    alias Library.Accounts.User
    alias Library.Repo
    alias Library.Logs.Method
    alias Library.UsersMethods

    def init(options), do: options

    def call(%{request_path: path, method: method} = conn, _opts) do
        case get_req_header(conn, "authorization") do
            ["Basic " <> auth] -> valid_auth(conn, auth)
            _ -> 
                case Repo.get_by(User, username: "anonymous") do
                    nil -> 
                        #Library.DummyData.push_dummy()
                    user ->
                        record_call(user, path, method)
                        conn
                end
                
        end
    end

    defp valid_auth(conn, auth) do
        {:ok, auth} = Base.decode64(auth)
        [user, password] = String.split(auth, ":")
        case Repo.get_by(User, username: user) do
            nil -> 
                #user does not exists
                conn
            user -> 
                if Bcrypt.verify_pass(password, user.password_hash) == true do
                    #user exists
                    %{request_path: path, method: method} = conn
                    record_call(user, path, method)
                    conn
                    |> assign(:user, user)
                else
                    #bad password
                    conn
                    |> send_resp(401, "wrong credentials or credentials needed but not provided")
                    |> halt()
                end
        end
    end

    defp record_call(user, function, method) do
        method = %Method{function: function, method: method}
        method = Repo.insert!(method)

        UsersMethods.changeset(%UsersMethods{}, %{user_id: user.id, method_id: method.id})
        |> Repo.insert!
    end

end