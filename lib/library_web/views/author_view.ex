defmodule LibraryWeb.AuthorView do
  use LibraryWeb, :view
  alias LibraryWeb.AuthorView

  def render("index.json", %{authors: authors}) do
    %{data: render_many(authors, AuthorView, "author.json")}
  end

  def render("show.json", %{author: author}) do
    %{data: render_one(author, AuthorView, "author.json")}
  end

  def render("show_all.json", %{author: author, books: books}) do
    %{data: 
      %{
        author: render_one(author, AuthorView, "author.json"),
        books: render_many(books, LibraryWeb.BookView, "book.json")
      }
    }
  end

  def render("author.json", %{author: author}) do
    %{id: author.id,
      firstname: author.firstname,
      lastname: author.lastname}
  end
end
