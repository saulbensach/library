defmodule LibraryWeb.BookView do
  use LibraryWeb, :view
  alias LibraryWeb.BookView

  def render("index.json", %{books: books}) do
    %{data: render_many(books, BookView, "book.json")}
  end

  def render("show.json", %{book: book}) do
    %{data: render_one(book, BookView, "book.json")}
  end

  def render("show_all.json", %{authors: authors, book: book}) do
    %{data: 
      %{
        book: render_one(book, BookView, "book.json"),
        authors: render_many(authors, LibraryWeb.AuthorView, "author.json")
      }
    }
  end

  def render("book.json", %{book: book}) do
    %{id: book.id,
      name: book.name,
      description: book.description}
  end
end
