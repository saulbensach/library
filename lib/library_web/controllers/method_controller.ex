defmodule LibraryWeb.MethodController do
  use LibraryWeb, :controller

  alias Library.Logs
  alias Library.Logs.Method

  action_fallback LibraryWeb.FallbackController

  def index(conn, _params) do
    methods = Logs.list_methods()
    render(conn, "index.json", methods: methods)
  end

  def create(conn, %{"method" => method_params}) do
    with {:ok, %Method{} = method} <- Logs.create_method(method_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.method_path(conn, :show, method))
      |> render("show.json", method: method)
    end
  end

  def show(conn, %{"id" => id}) do
    method = Logs.get_method!(id)
    render(conn, "show.json", method: method)
  end

  def update(conn, %{"id" => id, "method" => method_params}) do
    method = Logs.get_method!(id)

    with {:ok, %Method{} = method} <- Logs.update_method(method, method_params) do
      render(conn, "show.json", method: method)
    end
  end

  def delete(conn, %{"id" => id}) do
    method = Logs.get_method!(id)

    with {:ok, %Method{}} <- Logs.delete_method(method) do
      send_resp(conn, :no_content, "")
    end
  end
end
