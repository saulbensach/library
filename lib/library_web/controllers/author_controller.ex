defmodule LibraryWeb.AuthorController do
  use LibraryWeb, :controller

  import Ecto.Query

  alias Library.Libraries
  alias Library.Libraries.BooksAuthors
  alias Library.Repo
  alias Library.Libraries.Author
  alias Library.Libraries.Book

  action_fallback LibraryWeb.FallbackController

  def index(conn, _params) do
    authors = Libraries.list_authors()
    render(conn, "index.json", authors: authors)
  end

  def create(conn, %{"author" => author_params}) do
    with {:ok, %Author{} = author} <- Libraries.create_author(author_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.author_path(conn, :show, author))
      |> render("show.json", author: author)
    end
  end

  def show(conn, %{"id" => id}) do
    author = Libraries.get_author!(id)
    render(conn, "show.json", author: author)
  end

  def show_all(conn, %{"id" => id}) do
    author = Libraries.get_author!(id)
    query = from b in BooksAuthors,
      join: book in Book, on: book.id == b.book_id,
      where: b.author_id == ^id,
      select: %{
        id: book.id,
        name: book.name,
        description: book.description
      }
    authored_books = Repo.all(query)
    render(conn, "show_all.json", author: author, books: authored_books)
  end

  def update(conn, %{"id" => id, "author" => author_params}) do
    author = Libraries.get_author!(id)

    with {:ok, %Author{} = author} <- Libraries.update_author(author, author_params) do
      render(conn, "show.json", author: author)
    end
  end

  def delete(conn, %{"id" => id}) do
    author = Libraries.get_author!(id)

    with {:ok, %Author{}} <- Libraries.delete_author(author) do
      send_resp(conn, :no_content, "")
    end
  end
end
