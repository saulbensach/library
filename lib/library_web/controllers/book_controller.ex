defmodule LibraryWeb.BookController do
  use LibraryWeb, :controller

  import Ecto.Query
  alias Library.Libraries
  alias Library.Libraries.Book
  alias Library.Libraries.BooksAuthors
  alias Library.Repo
  alias Library.Libraries.Author

  action_fallback LibraryWeb.FallbackController

  def index(conn, _params) do
    books = Libraries.list_books()
    render(conn, "index.json", books: books)
  end

  def create(conn, %{"book" => book_params, "author" => author_id}) do
    with {:ok, %Book{} = book} <- Libraries.create_book(book_params) do
      changeset = BooksAuthors.changeset(%BooksAuthors{}, %{book_id: book.id, author_id: author_id})
      Repo.insert!(changeset) # if fails rollback
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.book_path(conn, :show, book))
      |> render("show.json", book: book)
    end
  end

  def show(conn, %{"id" => id}) do
    book = Libraries.get_book!(id)
    render(conn, "show.json", book: book)
  end

  def show_all(conn, %{"id" => id}) do
    book = Libraries.get_book!(id)
    query = from b in BooksAuthors,
      join: author in Author, on: author.id == b.author_id,
      where: b.book_id == ^id,
      select: %{
        id: author.id,
        firstname: author.firstname,
        lastname: author.lastname
      }
    authors_book = Repo.all(query)
    render(conn, "show_all.json", authors: authors_book, book: book)
  end

  def update(conn, %{"id" => id, "book" => book_params}) do
    book = Libraries.get_book!(id)

    with {:ok, %Book{} = book} <- Libraries.update_book(book, book_params) do
      render(conn, "show.json", book: book)
    end
  end

  def delete(conn, %{"id" => id}) do
    book = Libraries.get_book!(id)

    with {:ok, %Book{}} <- Libraries.delete_book(book) do
      send_resp(conn, :no_content, "")
    end
  end
end
