defmodule Library.AuthPlug do
    import Plug.Conn

    alias Library.AuthPlug
    alias Library.Accounts.User
    alias Library.Repo

    def init(options), do: options

    def call(conn, opts) do
        if Mix.env != :test do
            if(conn.assigns == %{}) do
                conn
                |> send_resp(401, "unauthorized")
                |> halt()
            else
                conn
            end
        else
            conn
        end
        
    end

end