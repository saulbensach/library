defmodule Library.Libraries.Book do
  use Ecto.Schema
  import Ecto.Changeset

  schema "books" do
    field :description, :string
    field :name, :string

    has_many :books_authors, Library.Libraries.BooksAuthors, on_delete: :delete_all
    timestamps()
  end

  @doc false
  def changeset(book, attrs) do
    book
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
  end
end
