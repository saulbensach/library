defmodule Library.Libraries.BooksAuthors do
    use Ecto.Schema
    import Ecto.Changeset

    schema "books_authors" do
        belongs_to :author, Library.Libraries.Author
        belongs_to :book, Library.Libraries.Book
    end

    def changeset(method, attrs) do
        method
        |> cast(attrs, [:author_id, :book_id])
        |> validate_required([:author_id, :book_id])
    end
end