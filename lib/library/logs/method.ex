defmodule Library.Logs.Method do
  use Ecto.Schema
  import Ecto.Changeset

  schema "methods" do
    field :function, :string
    field :method, :string

    has_many :users_methods, Library.UsersMethods, on_delete: :delete_all
    timestamps()
  end

  @doc false
  def changeset(method, attrs) do
    method
    |> cast(attrs, [:function, :method])
    |> validate_required([:function, :method])
  end
end
