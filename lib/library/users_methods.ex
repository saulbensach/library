defmodule Library.UsersMethods do
    use Ecto.Schema
    import Ecto.Changeset

    schema "users_methods" do
        belongs_to :user, Library.Accounts.User
        belongs_to :method, Library.Logs.Method
    end

    def changeset(method, attrs) do
        method
        |> cast(attrs, [:user_id, :method_id])
        |> validate_required([:user_id, :method_id])
    end

end