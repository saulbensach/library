defmodule Library.DummyData do
    alias Library.Repo
    alias Library.Accounts.User

    def push_dummy() do 
        if Repo.get_by(User, username: "anonymous") == nil do
            User.changeset(%User{}, %{username: "anonymous", password: "1231231231"})
            |> Repo.insert!
        end
        if Repo.get_by(User, username: "admin") == nil do
            User.changeset(%User{}, %{username: "admin", password: "admin"})
            |> Repo.insert!
        end
    end
end