defmodule Library.Repo.Migrations.AddMethodUser do
  use Ecto.Migration

  def change do
    create table(:users_methods) do
      add :user_id, references(:users)
      add :method_id, references(:methods)
    end
  end
end
