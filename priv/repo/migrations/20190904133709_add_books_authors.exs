defmodule Library.Repo.Migrations.AddBooksAuthors do
  use Ecto.Migration

  def change do
    create table(:books_authors) do
      add :author_id, references(:authors)
      add :book_id, references(:books)
    end
  end
end
