defmodule LibraryWeb.BookControllerTest do
    use LibraryWeb.ConnCase
  
    alias Library.Libraries
    alias Library.Libraries.Book
  
    @create_attrs %{
      description: "some description",
      name: "some name"
    }
    @update_attrs %{
      description: "some updated description",
      name: "some updated name"
    }
    @invalid_attrs %{description: nil, name: nil}
  
    def fixture(:book) do
      {:ok, book} = Libraries.create_book(@create_attrs)
      book
    end
  
    setup %{conn: conn} do
      {:ok, conn: put_req_header(conn, "accept", "application/json")}
    end

    test "create book!", %{conn: conn} do
        {:ok, author} = Libraries.create_author(%{firstname: "Saúl", lastname: "Bensach"})
        book_params = %{name: "best name", description: "this book is awesome"}
        response =
            conn
            |> post(Routes.book_path(conn, :create), %{"book": book_params, author: author.id})
            |> json_response(200)["data"]

        assert response["description"] == "this book is awesome"
        assert response["name"] == "name"
    end
  
    describe "index" do
      test "lists all books", %{conn: conn} do
        conn = get(conn, Routes.book_path(conn, :index))
        assert json_response(conn, 200)["data"] == []
      end
    end
  
    describe "update book" do
      setup [:create_book]
  
      test "renders book when data is valid", %{conn: conn, book: %Book{id: id} = book} do
        conn = put(conn, Routes.book_path(conn, :update, book), book: @update_attrs)
        assert %{"id" => ^id} = json_response(conn, 200)["data"]
  
        conn = get(conn, Routes.book_path(conn, :show, id))
  
        assert %{
                 "id" => id,
                 "description" => "some updated description",
                 "name" => "some updated name"
               } = json_response(conn, 200)["data"]
      end
  
      test "renders errors when data is invalid", %{conn: conn, book: book} do
        conn = put(conn, Routes.book_path(conn, :update, book), book: @invalid_attrs)
        assert json_response(conn, 422)["errors"] != %{}
      end
    end
  
    describe "delete book" do
      setup [:create_book]
  
      test "deletes chosen book", %{conn: conn, book: book} do
        conn = delete(conn, Routes.book_path(conn, :delete, book))
        assert response(conn, 204)
  
        assert_error_sent 404, fn ->
          get(conn, Routes.book_path(conn, :show, book))
        end
      end
    end
  
    defp create_book(_) do
      book = fixture(:book)
      {:ok, book: book}
    end
  end
  