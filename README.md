# Library

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`
  * When executing phoenix with `iex -S mix phx.server` execute   `Library.DummyData.push_dummy()` this creates and admin user with admin password
  and creates an anonymous user for obtaining data from GET requests
  (Wanted to create small genserver but runned out of time)

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Consuming JSON API

For all GET petitions there is no need to place authorization header
For all responses you need to place "application/json" header
Variables are identified with : Example :id, Replace variables with your needs

List all books
  + `GET /api/books/`
  + Returns 200

List a single book
  + `GET /api/books/:id`
  + Returns 200

List a single book and all of its authors
  + `GET /api/books/all/:id`
  + Returns 200

Create a new book with author
  + Header -> Basic Auth
  + `POST /api/books`
  + In HTTP Post body place: 
    `{
      "book": {
        "name": :name,
        "description": :description
      },
      "author": :id
    }`
    + Returns 201
    All books need at least one author you cannot create a book if an author does not exists

Updates an existing book
  + Header -> Basic Auth
  + `PUT /api/books/:id`
  + Body: 
    `{
        "book": {
          "name": :new_name,
          "description": :new_description
        }
      }`
  + Returns 200
Updates a certain parameter of a book
  + Header -> Basic Auth
  + `PATCH /api/books/:id`
  + Body: 
    `{
        "book": {
          "name": :new_name
        }
      }`
    OR
    `{
        "book": {
          "description": :new_description
        }
      }`
  + Returns 200

Delete a given book
  + Header -> Basic Auth
  + `DELETE /api/books/:id`
  + Returns 204

List all authors
  + `GET /api/authors/`
  + Returns 200

List a single author
  + `GET /api/authors/:id`
  + Returns 200

List a single author and all of its books
  + `GET /api/authors/all/:id`
  + Returns 200

Create a new author
  + Header -> Basic Auth
  + `POST /api/authors`
  + In HTTP Post body place: 
    `{
      "author": {
        "firstname": :firstname,
        "lastname": :lastname
      }
    }`
    + Returns 201

Updates an existing author
  + Header -> Basic Auth
  + `PUT /api/authors/:id`
  + Body: 
    `{
        "author": {
          "firstname": :firstname,
          "lastname": :lastname
        }
      }`
  + Returns 200

Updates a certain parameter of an author
  + Header -> Basic Auth
  + `PATCH /api/author/:id`
  + Body: 
    `{
        "author": {
          "firstname": :firstname
        }
      }`
    OR
    `{
        "author": {
          "lastname": :lastname
        }
      }`
  + Returns 200

Deletes an author
  + Header -> Basic Auth
  + `DELETE /api/author/:id`
  + Returns 204

List all users
  + `GET /api/users/`
  + Returns 200

List a single user
  + `GET /api/users/:id`
  + Returns 200

Create a new user
  + Header -> Basic Auth
  + `POST /api/user`
  + In HTTP Post body place: 
    `{
      "user": {
        "username": :username,
        "password": :password
      }
    }`
  + Returns 201

Endpoints for updating user are disabled if you want to be able to edit them
go to router.ex and uncomment the routes

Deletes an user
  + Header -> Basic Auth
  + `DELETE /api/user/:id`
  + Returns 204

List all methods
  + `GET /api/methods/`
  + Returns 200

List a single method
  + `GET /api/methods/:id`
  + Returns 200

Endpoints for DELETE, PUT, PATH, POST are disabled. You can enable them by uncommenting them on router.ex